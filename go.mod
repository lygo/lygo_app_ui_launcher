module bitbucket.org/lygo/lygo_app_ui_launcher

go 1.14

require (
	bitbucket.org/lygo/lygo_commons v0.1.98
	bitbucket.org/lygo/lygo_events v0.1.8
	bitbucket.org/lygo/lygo_ext_logs v0.1.6
	bitbucket.org/lygo/lygo_resources v0.1.2
	bitbucket.org/lygo/lygo_updater v0.1.27
	github.com/go-gl/glfw/v3.3/glfw v0.0.0-20210410170116-ea3d685f79fb // indirect
	github.com/hajimehoshi/ebiten v1.12.12
	golang.org/x/exp v0.0.0-20210607182018-cd2df34ff7e5 // indirect
	golang.org/x/image v0.0.0-20210607152325-775e3b0c77b9 // indirect
	golang.org/x/mobile v0.0.0-20210527171505-7e972142eb43 // indirect
	golang.org/x/sys v0.0.0-20210608053332-aa57babbf139 // indirect
)
