# Screen Launcher #
![icon](./_icons.iconset/icon_256x256.png)

Launch a "loading" screen while your application is starting and initializing.

Some time applications needs a while to start. Screen Launcher allow your users understand that your application is starting end is not locked.

## Sample Screenshot ##
![screenshot](./screenshot.png)

Screen Launcher is fully customizable with your image and your custom message.

## Binaries ##
* Mac: Mac binaries are [here](./build/mac). Just copy ScreenLauncher.app. A Sample installer is [here](./_dmg).
* Windows: Windows binaries are [here](./build/windows).
* Linux: Coming soon

## Configuration ##

Screen Launcher is fully configurable changing file init.json.

```
{
  "image": "",
  "exit_file_name": "./launched",
  "exit_file_reset_on_start": true,
  "timeout": "30",
  "wait_text": "Please, wait... $time",
  "command_to_run": "open $dir_home/Mypp.app",

  "version_file": "https://gianangelogeminiani.me/download/version.txt",
  "package_files": [
    {
      "file": "https://gianangelogeminiani.me/download/package.zip",
      "target": "./_download"
    }
  ]
}
```

**Parameters**

* image: Path (relative or absolute) to background image. If not specified is used a default image. The screen loader size is same as image size (default image size is 600x313). 
* exit_file_name: If this file exists, the launcher close itself. Works both with relative or absolute paths. The file can be empty. Usually this file is created from external application to notify the launcher to close.
* exit_file_reset_on_start: Remove "exit_file_name" at start. Used to reset status if you need the screen load every time at launch.
* command_to_run: Command to run when screen launcher is active. Use this to run your program. 
* timeout: Wait timeout in seconds if "exit_file_name" is not found. The screen launcher should close itself after a max amount of time.
* wait_text: Overlay text to display.

**Updater Parameters**

* version_file: Path (relative or absolute) to background image. If not specified is used a default image. The screen loader size is same as image size (default image size is 600x313). 
* package_files: Array of objects (PackageFile) to download and unzip (if archive). PackageFile contains "file" and "target" fields.
* command_to_run: Command to run when screen launcher is active. Use this to run your program. 


**Variables**

Some parameters (`command_to_run` and `wait_text`) can contain variables.
* $time: Is replaced with current time (hour:minute seconds)
* $datetime: Is replaced with current date and time.
* dir_home: Is replaced with Application absolute path.

## Dependencies

`go get -u bitbucket.org/lygo/lygo_commons`
`go get -u bitbucket.org/lygo/lygo_updater`