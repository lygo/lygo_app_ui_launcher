package screen

import (
	"bitbucket.org/lygo/lygo_app_ui_launcher/resources"
	"bitbucket.org/lygo/lygo_commons/lygo_conv"
	"bitbucket.org/lygo/lygo_commons/lygo_fmt"
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_commons/lygo_json"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"bitbucket.org/lygo/lygo_events"
	"bitbucket.org/lygo/lygo_ext_logs"
	"bitbucket.org/lygo/lygo_updater/src"
	"bytes"
	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/ebitenutil"
	"image"
	"os"
	"strings"
	"time"
)

//----------------------------------------------------------------------------------------------------------------------
//	t y p e
//----------------------------------------------------------------------------------------------------------------------

const DEFAULT_TIMEOUT = 30 * time.Second

type Screen struct {
	ImageName            string
	ExitFileName         string
	ExitFileResetOnStart bool
	Timeout              time.Duration
	WaitText             string
	CommandToRun         string

	//-- private --//
	settings  *Settings
	img       *ebiten.Image
	width     int
	height    int
	chanClose chan bool
}

type Settings struct {
	Image                string `json:"image"`
	ExitFileName         string `json:"exit_file_name"`
	ExitFileResetOnStart bool   `json:"exit_file_reset_on_start"`
	Timeout              string `json:"timeout"`
	WaitText             string `json:"wait_text"`
	lygo_updater.Settings
}

//----------------------------------------------------------------------------------------------------------------------
//	c o n s t r u c t o r
//----------------------------------------------------------------------------------------------------------------------

func NewScreen(settings string) *Screen {
	instance := new(Screen)
	instance.settings = new(Settings)
	lygo_json.Read(settings, instance.settings)

	instance.ImageName = instance.settings.Image
	instance.ExitFileName = instance.settings.ExitFileName
	instance.ExitFileResetOnStart = instance.settings.ExitFileResetOnStart
	if len(instance.settings.Timeout) > 0 {
		instance.Timeout = time.Duration(lygo_conv.ToInt(instance.settings.Timeout)) * time.Second
	} else {
		instance.Timeout = DEFAULT_TIMEOUT
	}
	instance.WaitText = instance.settings.WaitText
	instance.CommandToRun = instance.settings.CommandToRun

	instance.width = 640
	instance.height = 480
	instance.loadImage()

	instance.chanClose = make(chan bool, 1)

	return instance
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *Screen) Update(screen *ebiten.Image) error {
	return nil
}

func (instance *Screen) Draw(screen *ebiten.Image) {
	// background image
	if nil != instance.img {
		op := &ebiten.DrawImageOptions{}
		op.GeoM.Translate(0, 0)
		op.GeoM.Scale(1, 1)
		_ = screen.DrawImage(instance.img, op)
	}

	if len(instance.WaitText) > 0 {
		txt := replaceVars(instance.WaitText)
		ebitenutil.DebugPrintAt(screen, txt, instance.width-(10+len(txt)*6), instance.height-30)
	}
}

func (instance *Screen) Layout(outsideWidth, outsideHeight int) (int, int) {
	return instance.width, instance.height
}

func (instance *Screen) Show() {

	ebiten.SetWindowSize(instance.width, instance.height)
	ebiten.SetWindowTitle("")
	ebiten.SetWindowResizable(false)
	ebiten.SetWindowDecorated(false)
	ebiten.SetWindowFloating(false)

	if instance.ExitFileResetOnStart && len(instance.ExitFileName) > 0 {
		_ = lygo_io.Remove(instance.ExitFileName)
	}

	go start(instance)

	if err := ebiten.RunGame(instance); err != nil {
		lygo_ext_logs.Error(err)
	}
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *Screen) loadImage() {
	// load image
	if len(instance.ImageName) > 0 {
		if b, err := lygo_paths.Exists(instance.ImageName); b && nil == err {
			i, _, err := ebitenutil.NewImageFromFile(instance.ImageName, ebiten.FilterDefault)
			if err != nil {
				lygo_ext_logs.Error("loadImage", err)
			} else {
				if nil != i {
					instance.setImage(i)
					return // exit
				}
			}
		}
	}

	// load default image
	if data, b := resources.Get("src_resources/screen.png"); b {
		img, _, err := image.Decode(bytes.NewReader(data))
		if err != nil {
			lygo_ext_logs.Error("loadImage", err)
		}
		i, err := ebiten.NewImageFromImage(img, ebiten.FilterDefault)
		if nil == err && nil != i {
			instance.setImage(i)
			return // exit
		}
	}
}

func (instance *Screen) setImage(img *ebiten.Image) {
	instance.img = img
	w, h := img.Size()
	instance.setSize(w, h)
}

func (instance *Screen) setSize(w, h int) {
	instance.width = w
	instance.height = h
}

//----------------------------------------------------------------------------------------------------------------------
//	S T A T I C
//----------------------------------------------------------------------------------------------------------------------

func replaceVars(text string) string {
	txt := strings.Replace(text, "$time", lygo_fmt.FormatDate(time.Now(), "HH:mm ss"), -1)
	txt = strings.Replace(txt, "$datetime", lygo_fmt.FormatDate(time.Now(), "YYYY-MM-dd HH:mm ss"), -1)

	txt = strings.Replace(txt, "$dir_home", lygo_paths.Absolute("./"), -1)

	return txt
}

func start(screen *Screen) {
	go wait(screen.chanClose)

	// little initial delay
	time.Sleep(3 * time.Second)

	// updater and app launcher
	upd := lygo_updater.NewUpdater(screen.settings.Settings)
	_, _, _, _, err := upd.Start()
	if nil != err {
		lygo_ext_logs.Error("start", err)
	}
	// done, err := runCommand(screen.CommandToRun)

	// wait for exit file
	fileName := screen.ExitFileName
	if len(fileName) > 0 {
		// loop waiting for file
		timeout := screen.Timeout
		now := time.Now()
		ticker := lygo_events.NewEventTicker(1*time.Second, func(t *lygo_events.EventTicker) {
			if time.Now().Sub(now) > timeout {
				t.Stop() // exit
			}
			if b, err := lygo_paths.Exists(fileName); b || nil != err {
				t.Stop() // exit loop
			}
		})
		ticker.Join() // wait
		// quit
		screen.chanClose <- true
	} else {
		time.Sleep(10 * time.Second)
	}

	// quit
	screen.chanClose <- true
}

func wait(c chan bool) {
	<-c
	os.Exit(0)
}
