package main

import (
	"bitbucket.org/lygo/lygo_commons/lygo_exec"
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"bitbucket.org/lygo/lygo_commons/lygo_reflect"
	"bitbucket.org/lygo/lygo_resources"
	"fmt"
	"testing"
	"time"
)

const packageName = "resources"
const startDirectory = "src_resources"

// run resource packer and creates a resource binary
func TestResourcePacker(t *testing.T) {
	// run generator
	generator := lygo_resources.NewGenerator()
	generator.Package = packageName
	generator.StartDirectory = startDirectory
	// generator.OutputFile = "./" + packageName + "/blob_{{ .count }}.go"
	generator.Exclude = []string{"launched"}
	generator.ForceSingleResourceFile = true // creates a single file ignoring custom "OutputFile" param
	generator.Start()
}

func TestLaunchAndClose(t *testing.T) {

	settings := loadSettings()
	closefile := lygo_reflect.GetString(settings, "exit_file_name")

	root := lygo_paths.Absolute("./build/mac/ScreenLauncher.app/Contents")
	app := lygo_paths.Concat(root, "MacOS/ScreenLauncher")
	closefile = lygo_paths.Concat(root, "Resources", closefile)

	// run app
	fmt.Println("LAUNCH SCREEN")

	go lygo_exec.RunBackground(app)

	time.Sleep(3 * time.Second)

	fmt.Println("CREATING FILE")

	_, err := lygo_io.WriteTextToFile("", closefile)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println("CREATED FILE")
}
