package main

import (
	"bitbucket.org/lygo/lygo_app_ui_launcher/screen"
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"bitbucket.org/lygo/lygo_ext_logs"
)

func main() {

	lygo_paths.SetWorkspacePath("./")
	lygo_ext_logs.SetLevel(lygo_ext_logs.LEVEL_INFO)
	lygo_ext_logs.SetOutput(lygo_ext_logs.OUTPUT_FILE)

	window := screen.NewScreen(loadSettings())
	window.Show()
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func loadSettings() string {
	text, err := lygo_io.ReadTextFromFile("./init.json")
	if nil == err {
		return text
	} else {
		lygo_ext_logs.Error(err)
	}
	return ""
}
