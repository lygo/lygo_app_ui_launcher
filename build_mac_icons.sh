##!/bin/sh
SIZE=16
sips -z $SIZE $SIZE icon.png --out ./_icons.iconset/icon_${SIZE}x${SIZE}.png

SIZE=32
sips -z $SIZE $SIZE icon.png --out ./_icons.iconset/icon_${SIZE}x${SIZE}.png
sips -z $SIZE $SIZE icon.png --out ./_icons.iconset/icon_16x16@2x.png

SIZE=64
sips -z $SIZE $SIZE icon.png --out ./_icons.iconset/icon_${SIZE}x${SIZE}.png
sips -z $SIZE $SIZE icon.png --out ./_icons.iconset/icon_32x32@2x.png

SIZE=128
sips -z $SIZE $SIZE icon.png --out ./_icons.iconset/icon_${SIZE}x${SIZE}.png
sips -z $SIZE $SIZE icon.png --out ./_icons.iconset/icon_64x64@2x.png

SIZE=256
sips -z $SIZE $SIZE icon.png --out ./_icons.iconset/icon_${SIZE}x${SIZE}.png
sips -z $SIZE $SIZE icon.png --out ./_icons.iconset/icon_128x128@2x.png

SIZE=512
sips -z $SIZE $SIZE icon.png --out ./_icons.iconset/icon_${SIZE}x${SIZE}.png
sips -z $SIZE $SIZE icon.png --out ./_icons.iconset/icon_256x256@2x.png

SIZE=1024
sips -z $SIZE $SIZE icon.png --out ./_icons.iconset/icon_${SIZE}x${SIZE}.png
sips -z $SIZE $SIZE icon.png --out ./_icons.iconset/icon_512x512@2x.png

iconutil -c icns -o ./build/mac/ScreenLauncher.app/Contents/Resources/icon.icns ./_icons.iconset